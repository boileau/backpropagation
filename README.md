# Rétropropagation du gradient dans les réseaux de neurones à propagation avant

## Installation

Prérequis : python >= 3.6

```bash
python3 -m pip install -r requirements.txt
```

## Exécution du cours sous forme de notebooks jupyter

Lancer un serveur Jupyter local :

```bash
jupyter-notebook backpropagation.ipynb
```

Tester en ligne : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fboileau%2Fbackpropagation/master?filepath=backpropagation.ipynb)

## Utilisation du programme python

```
~$ python3 backpropation.py -h

NAME
    backpropagation.py - Entraînement d'un réseau perceptron à deux couches sur un problème de classification

SYNOPSIS
    backpropagation.py <flags>

DESCRIPTION
    Entraînement d'un réseau perceptron à deux couches sur un problème de classification

FLAGS
    --Pattern=PATTERN
        Default: <class '__main__.Diamond'>
    --n_obs=N_OBS
        Default: 500
    --Activation=ACTIVATION
        Default: <class '__main__.Sigmoid'>
    --learning_rate=LEARNING_RATE
        Default: 0.001
    --seed=SEED
        Default: 1241
    --n_iter=N_ITER
        Default: 5000
    --n_iter_out=N_ITER_OUT
        Default: 50
    --animate=ANIMATE
        Default: True
    --save=SAVE
        Default: False
    --verbose=VERBOSE
        Default: False
```

On peut exporter l'animation en une suite d'images png dans le répertoire `png/` :

```
~$ python3 backpropation.py --save
```

Puis créer un gif animé de l'entrainement avec la commande (MacOS) :

```bash
convert png/ite*.png training.gif
```

![animation d'apprentissage](training.gif)
